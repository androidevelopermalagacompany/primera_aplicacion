package clases;

import java.io.Serializable;
import java.util.ArrayList;

/** Clase que representa a un usuario que esta en una casa
 * @author Juan Carlos Beltran
 */
public class Usuario implements Serializable{
    private String nombre; //@param nombre que representa el nombre del usuario
    private String pass; //@param pass que representa la contraseña del usuario
    private ArrayList<Casa> casas; //@param casas que representa la lista de casas del usuario, en principio solo va a poder tener 3

    /**
     * Usuario (Constructor) Construye un usuario sin ningun parametro
     * @TODO Esto deberia estar vacio pero creo una casa y una tarea de prueba
     */
    public Usuario() {
    /*        ArrayList<Usuario> usuarios = new ArrayList<>();
        usuarios.add(this);

        casas = new ArrayList<>();
        casas.add(new Casa("Casa", usuarios, "1234"));
        */
    }

    /**
     * Usuario (Constructor) Construye un usuario a partir de un nombre, contraseña y ArrayList de casas
     * @param nombre que representa el nombre del usuario
     * @param pass que representa la contraseña del usuario
     * @param casas que representa la lista de casas del usuario
     */
    public Usuario(String nombre, String pass, ArrayList<Casa> casas) {
        this.nombre = nombre;
        this.pass = pass;
        this.casas = casas;
    }

    public Usuario(String nombre, String pass) {
        this.nombre = nombre;
        this.pass = pass;
    }

    /**
     * metodo de devuelve el nombre del usuario
     * @return String nombre del usuario
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * metodo que cambia el nombre del usuario
     * @param nombre nombre String por el que se cambia el nombre del usuario
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * metodo que devuelve la contraseña del usuario
     * @return String contraseña del usuario
     */
    public String getPass() {
        return pass;
    }

    /**
     * metodo que cambia la contraseña del usuario
     * @param pass pass String por el que se cambia la contraseña del usuario
     */
    public void setPass(String pass) {
        this.pass = pass;
    }

    /**
     * metodo que devuelve el ArrayList de casas del usuario
     * @return ArrayList<Casa> lista de casas del usuario
     */
    public ArrayList<Casa> getCasas() {
        return casas;
    }

    /**
     * metodo que cambia la lista de casas del usuario, en principio solo va a poder tener 3
     * @param casas casas ArrayList<Casa> por el que se cambia la lista de casas del usuario
     */
    public void setCasas(ArrayList<Casa> casas) {
        this.casas = casas;
    }
}
