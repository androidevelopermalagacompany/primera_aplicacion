package com.example.orghogar.orghogar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

/**Esta es la ventana para añadir tareas dos, en donde en esta ocasión se asignará de modo aleatorio las tareas a los usuarios.
 * @author Ismael Burgos García
 */

public class VentanaAnadirTarea2 extends AppCompatActivity {

    ImageButton btnVolver; //@param imagen del boton volver.


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ventana_anadir_tarea2);
    }

    /**
     * Evento Onclick en donde la imagen boton volver referenciará hacia esta ventana y se relacionará con la ventana de añadir tarea 1.
     *
     * @param view
     */

    // ONCLICK VOLVER
    public void volver(View view) {
        Intent btnVolver = new Intent(this, VentanaAnadirTarea.class);
        startActivity(btnVolver);
    }
}


    //TODO hacer el boton que asigna la tarea aleatoreamente entre los  usuarios

