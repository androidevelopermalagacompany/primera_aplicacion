package com.example.orghogar.orghogar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import clases.Usuario;

/** Ventana en el cual el usuario debe de registrarse, si nunca se ha dado de alta.
 * @author Ismael Burgos García
 * @author Juan Carlos Beltran
 */


public class VentanaRegistrarse extends AppCompatActivity {
    EditText nombre;
    EditText contrasena;
    EditText correo;
    CheckBox aceptar;
    ImageButton btnvolver; //@param Esto es una imagen del boton que hemos asignado para volver.


    /**
     * OnCreate en el cual se asigna el boton volver su id correspondiente.
     * @param savedInstanceState
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ventana_registrarse);

        nombre = findViewById(R.id.editText_nombre);
        contrasena = findViewById(R.id.editText_registrarse_contrasena);
        correo = findViewById(R.id.editText_correo);
        btnvolver =(ImageButton)findViewById(R.id.imageButton10);
        aceptar = findViewById(R.id.checkBox_acepto_terminos);
    }


    /**
     * Intent en donde asignamos la referencia de la ventana de registro hacia la ventana principal
     * @param view
     */

    // ONCLICK VOLVER DEL REGISTRO

    public void botonvolver(View view) {
        Intent btnvolver=new Intent(VentanaRegistrarse.this,VentanaPrincipal.class);
        startActivity(btnvolver);
    }


    /** Clase que registra al usuario en la base de datos Firebase y se mueve a la ventana login
     * @author Juan Carlos Beltran
     * @param view
     */
    public void botonRegistrarse(View view) {
        if(!nombre.getText().toString().equals("") && !contrasena.getText().toString().equals("") && !correo.getText().toString().equals("") && aceptar.isChecked()) {
            Usuario u = new Usuario(nombre.getText().toString(), contrasena.getText().toString());
            DatabaseReference myRef = FirebaseDatabase.getInstance().getReference();
            myRef.child(nombre.getText().toString()).setValue(u);

            Intent haciaVentanaLogin = new Intent(this, VentanaLogin.class);
            Bundle datos = new Bundle();
            datos.putSerializable("usuario", u);
            haciaVentanaLogin.putExtras(datos);
            startActivity(haciaVentanaLogin);
        }
    }
}
