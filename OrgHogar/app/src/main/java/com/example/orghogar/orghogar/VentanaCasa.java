package com.example.orghogar.orghogar;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import clases.Usuario;

/** Ventana donde el usuario elige la casa o añade una nueva
 * @author Juan Carlos Beltran
 */

public class VentanaCasa extends AppCompatActivity {
    private Usuario u; //@param u Usuario que ha iniciado sesion en la aplicacion
    private ImageButton botonVolver; //@param botonVolver ImageButton para volver a la ventana anterior
    private ImageView imagenCasa1; //@param imagenCasa1 ImageView de la casa 1
    private TextView textCasa1; //@param textCasa1 TextView de la casa 1
    private ImageView imagenCasa2; //@param imagenCasa2 ImageView de la casa 3
    private TextView textCasa2; //@param textCasa2 TextView de la casa 3
    private ImageView imagenCasa3; //@param imagenCasa3 ImageView de la casa 2
    private TextView textCasa3; //@param textCasa2 TextView de la casa 3
    private ImageView imagenMas; //@param imagenMas ImageView para añadir una nueva casa

    /**
     * onCreate que trae el objeto Usuario serializado del intent o del savedInstanceState y detecta si el usuario tiene casas y las muestra en la ventana
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ventana_casa);

        botonVolver = findViewById(R.id.imageButton5);

        if (this.getIntent().getExtras() != null) {
            u = (Usuario) this.getIntent().getExtras().getSerializable("usuario");
            //Si el usuario tiene alguna casa, mueve el boton de añadir usuario y lo cambia de tamaño, luego hace visibles las casas que tiene el usuario dependiendo de cuantas tenga
            if (u.getCasas().size() == 1) {
                ajustarBotonMas();
                hacerVisibleCasa1();
            } else if (u.getCasas().size() == 2) {
                ajustarBotonMas();
                hacerVisibleCasa1();
                hacerVisibleCasa2();
            } else if (u.getCasas().size() == 3) {
                ajustarBotonMas();
                hacerVisibleCasa1();
                hacerVisibleCasa2();
                hacerVisibleCasa3();
            }
        } else if (savedInstanceState != null) {
            u = (Usuario) (savedInstanceState.getSerializable("usuario"));
        }
        else {
            u = new Usuario();
        }
    }

    /**
     * Hace un intent que apunta hacia la ventana del login con un bundle con el usuario
     * @param view
     */
    public void clickBotonAtras(View view) {
        Intent i = new Intent(this, VentanaLogin.class);
        Bundle miBundle = new Bundle();
        miBundle.putSerializable("usuario", u);
        i.putExtras(miBundle);
        startActivity(i);
    }

    /**
     * Metodo que ajusta el boton de añadir para hacerlo mas pequeño y ajustarlo bien
     */
    private void ajustarBotonMas() {
        TextView advertencia = findViewById(R.id.textView_advertencia);
        advertencia.setVisibility(View.INVISIBLE);
        imagenMas = findViewById(R.id.imageView_mas);
        imagenMas.getLayoutParams().height = (int) getResources().getDimension(R.dimen.ventanacasa_imageviewmas_height);
        imagenMas.getLayoutParams().width = (int) getResources().getDimension(R.dimen.ventanacasa_imageviewmas_width);
        ConstraintLayout.LayoutParams lp = new ConstraintLayout.LayoutParams(imagenMas.getLayoutParams());
        lp.setMargins(0, 0, 0, 0);
        imagenMas.setLayoutParams(lp);
    }

    /**
     * Metodo que hace visible la imagen y el texto de la casa 1
     */
    private void hacerVisibleCasa1() {
        imagenCasa1 = findViewById(R.id.imageView_casa1);
        textCasa1 = findViewById(R.id.textView_casa1);
        textCasa1.setText(u.getCasas().get(0).getNombre());
        imagenCasa1.setVisibility(View.VISIBLE);
        textCasa1.setVisibility(View.VISIBLE);
    }

    /**
     * Metodo que hace visible la imagen y el texto de la casa 2
     */
    private void hacerVisibleCasa2() {
        imagenCasa2 = findViewById(R.id.imageView_casa2);
        textCasa2 = findViewById(R.id.textView_casa2);
        textCasa1.setText(u.getCasas().get(1).getNombre());
        imagenCasa2.setVisibility(View.VISIBLE);
        textCasa2.setVisibility(View.VISIBLE);
    }

    /**
     * Metodo que hace visible la imagen y el texto de la casa 3
     */
    private void hacerVisibleCasa3() {
        imagenCasa3 = findViewById(R.id.imageView_casa3);
        textCasa3 = findViewById(R.id.textView_casa3);
        textCasa1.setText(u.getCasas().get(2).getNombre());
        imagenCasa3.setVisibility(View.VISIBLE);
        textCasa3.setVisibility(View.VISIBLE);
    }

    /**
     * Hace un intent que apunta hacia la ventana de crear casa con un bundle con el usuario
     * @param view
     */
    public void clickAnadirCasa(View view) {
        if (u.getCasas().size() < 3) {
            Intent i = new Intent(this, VentanaInsertarCasa.class);
            Bundle miBundle = new Bundle();
            miBundle.putSerializable("usuario", u);
            i.putExtras(miBundle);
            startActivity(i);
        } else {
            Toast.makeText(this, "Solo puedes tener 3 casas como máximo", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Hace un intent que apunta hacia la ventana de tareas con un bundle con el usuario y la casa 1
     * @param view
     */
    public void clickCasa1(View view) {
        Intent i = new Intent(this, VentanaMostrarTareas.class);
        Bundle miBundle = new Bundle();
        miBundle.putSerializable("usuario", u);
        miBundle.putSerializable("casa", u.getCasas().get(0));
        i.putExtras(miBundle);
        startActivity(i);
    }

    /**
     * Hace un intent que apunta hacia la ventana de tareas con un bundle con el usuario y la casa 2
     * @param view
     */
    public void clickCasa2(View view) {
        Intent i = new Intent(this, VentanaMostrarTareas.class);
        Bundle miBundle = new Bundle();
        miBundle.putSerializable("usuario", u);
        miBundle.putSerializable("casa", u.getCasas().get(1));
        i.putExtras(miBundle);
        startActivity(i);
    }

    /**
     * Hace un intent que apunta hacia la ventana de tareas con un bundle con el usuario y la casa 3
     * @param view
     */
    public void clickCasa3(View view) {
        Intent i = new Intent(this, VentanaMostrarTareas.class);
        Bundle miBundle = new Bundle();
        miBundle.putSerializable("usuario", u);
        miBundle.putSerializable("casa", u.getCasas().get(2));
        i.putExtras(miBundle);
        startActivity(i);
    }
}
