package com.example.orghogar.orghogar;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import clases.Usuario;


/** Ventana añadir tarea uno, en donde se dirá el nombre de la tarea del usuario y se dirá cuantas veces es repetida la acción.
 * @author Ismael Burgos García
 */

public class VentanaAnadirTarea extends AppCompatActivity {
    private Usuario u; //@param u Usuario que ha iniciado sesion en la aplicacion

    Button Siguiente; //@param boton siguiente.
    ImageButton volverTarea;//@param imagen del boton volver.

    /**
     * OnCreate donde vamos a indicar los id tanto del boton siguiente y de la imagen del boton volver a tarea.
     * @param savedInstanceState
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ventana_anadir_tarea);

        Siguiente=(Button)findViewById(R.id.boton_siguiente);
        volverTarea=(ImageButton)findViewById(R.id.imageButton_ir_atras);

    }


    /**
     * Evento en el cual el boton siguiente referencia a la ventana de añadir tarea 1 hacia ventana añadir tarea 2.
     * @param view
     */

    // ONCLICK Siguiente
    public void botonSiguiente(View view) {
        Intent btn=new Intent(this,VentanaAnadirTarea2.class);
        startActivity(btn);
    }

    /**
     * Evento en el cual la imagen del boton volver tarea  referencia a la ventana de añadir tarea 1 hacia la ventana de tareas.
     * @param view
     */

    // ONCLICK volver tarea

    public void volverAtras(View view) {
        Intent btn=new Intent(this,VentanaTareas.class);
        startActivity(btn);
    }


}
