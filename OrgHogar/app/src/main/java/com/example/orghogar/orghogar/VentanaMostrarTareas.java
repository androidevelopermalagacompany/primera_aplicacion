package com.example.orghogar.orghogar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

/**
 * Author Amariliz Navarro
 * Esta clase es una intermediaria entre la clase VentanaCasa con dos opciones  Ver tus tareas de la clase VentanaTusTareas
 * o Añadir Tarea de la clase VentanaAnadirTarea
 *
 */

public class VentanaMostrarTareas extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ventana_mostrar_tareas);
    }

    /**
     *Hace un intent que apunta a ventana tus tareas llevandolo allí
     * @param view
     */
    //ONCLICK ir a la clase ventanaTusTareas
    public void clickTusTareas(View view) {
        Intent clickTusTareas =new Intent(this, VentanaTusTareas.class);
        startActivity(clickTusTareas);
    }

    /**
     * Hace un intent que apunta a ventana añadir tarea llevandolo alli
     * @param view
     */
    //ONCLICK ir a  la clase ventanaAnadirTarea
    public void clickAnadirTarea(View view) {
        Intent clickAnadirTarea=new Intent(this,VentanaAnadirTarea.class);
        startActivity(clickAnadirTarea);
    }
}
