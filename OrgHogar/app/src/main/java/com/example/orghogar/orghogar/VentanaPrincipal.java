package com.example.orghogar.orghogar;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;


/** Ventana en donde el usuario interectuará las dos opciones iniciar sesión o registrarse.
 * @author Ismael Burgos García
 * @author Juan Carlos Beltran
 */

public class VentanaPrincipal extends AppCompatActivity {
    ImageView movil;
    Button botonIniciarSesion; // @param Este es el boton en donde el usuario podrá iniciar sesión.
    Button botonRegistrarse; // @param Este es el boton en donde el usuario podrá registrarse.
    SharedPreferences preferences; //Aquí uso un fichero de preferencias diferente al que hay por defecto
    boolean modoVibracion; //donde guardo la preferencia de si se usa el modo noche


    /**
     * OnCreate de la ventana principal en donde se asigna los id de ambos botones. Si el modo vibracion esta activado el movil vibrará
     * @param savedInstanceState
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ventana_principal);

        botonIniciarSesion =(Button)findViewById(R.id.button_iniciar_sesion);
        botonRegistrarse =(Button)findViewById(R.id.button_registrarse);

        preferences = this.getSharedPreferences("principales", Context.MODE_PRIVATE);
        modoVibracion = preferences.getBoolean(this.getResources().getResourceName(R.string.opcionVibracion), false);
        movil = findViewById(R.id.imageView_movil_vibracion);
        if (modoVibracion) {
            movil.setImageResource(R.mipmap.movil_vibracion);
            Vibrator vibrator = (Vibrator)getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(1000);
        } else {
            movil.setImageResource(R.mipmap.movil_no_vibracion);
        }
    }


    /**
     * Hace un intent que va referenciado de la ventana principal hacia la ventana de login, en el cual se inicializa al boton iniciar sesion.
     * @param view
     */

  // ONCLICK INICIO DE SESIÓN

    public void IniciarSesion(View view) {
        Intent botonIniciarSesion=new Intent(VentanaPrincipal.this,VentanaLogin.class);
        startActivity(botonIniciarSesion);
        }


    /**
     * Hace un intent que va referenciado de la ventana principal hacia la ventana de registro en el cual se inicializa al boton registrarse.
     * @param view
     */

    // ONCLICK REGISTRARSE

    public void Registrarse(View view) {
        Intent botonRegistrarse =new Intent(VentanaPrincipal.this,VentanaRegistrarse.class);
        startActivity(botonRegistrarse);
    }

    public void clickVibracion(View view) {
        SharedPreferences.Editor editor = preferences.edit();
        if(modoVibracion){
            editor.putBoolean(this.getResources().getResourceName(R.string.opcionVibracion),false);
            movil.setImageResource(R.mipmap.movil_no_vibracion);
            modoVibracion=false;
        }else{
            editor.putBoolean(this.getResources().getResourceName(R.string.opcionVibracion),true);
            movil.setImageResource(R.mipmap.movil_vibracion);
            modoVibracion=true;
            Vibrator vibrator = (Vibrator)getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(1000);
        }
        editor.commit();
    }
}

