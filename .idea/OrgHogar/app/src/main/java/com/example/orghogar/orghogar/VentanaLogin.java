package com.example.orghogar.orghogar;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import clases.Casa;
import clases.FlagsOrgHogar;
import clases.Usuario;

/** Ventana donde el usuario inicia sesion
 * @author Juan Carlos Beltran
 * @author Ismael
 */

public class VentanaLogin extends AppCompatActivity {
    private Usuario u; //@param u Usuario que va a iniciado sesion en la aplicacion
    private Usuario u2;
    private EditText nombre; //@param nombre EditText donde el usuario inserta su nombre
    private EditText pass; //@param pass EditText donde el usuario inserta su contraseña
    private ImageButton btnvolver; //@param btnvolver ImageButton para volver a la ventana anterior

    /**
     * onCreate que trae el objeto Usuario serializado del intent o del savedInstanceState
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ventana_login);

        u2= new Usuario("", "");

        nombre = findViewById(R.id.editText_apellido);
        pass = findViewById(R.id.editText_contrasena);
        btnvolver = findViewById(R.id.imageButton);

        if (this.getIntent().getExtras() != null) {
            u = (Usuario) this.getIntent().getExtras().getSerializable("usuario");
            nombre.setText(u.getNombre());
            pass.setText(u.getPass());
        } else if (savedInstanceState != null) {
            u = (Usuario) (savedInstanceState.getSerializable("usuario"));
            nombre.setText(u.getNombre());
            pass.setText(u.getPass());
        }
        else {
            u = new Usuario("Anonimo", "1234");
        }
    }

    /**
     * Hace un intent que apunta hacia la ventana principal
     * @param view
     */
    public void botonvolver(View view) {
        Intent btnvolver=new Intent(VentanaLogin.this,VentanaPrincipal.class);
        startActivity(btnvolver);
    }

    /**
     * Metodo para el click del boton para iniciar sesion que te lleva a la ventana de casas.
     */
    public void clickIniciarSesion(View view) {
        if(!nombre.getText().toString().equals("") && !pass.getText().toString().equals("")) {
            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR)==PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR)==PackageManager.PERMISSION_GRANTED){

                DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(nombre.getText().toString());
                myRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        u2 = dataSnapshot.getValue(Usuario.class);
                        // Log.d("consulta", "Value is: " + u.getNombre());
                    }

                    @Override
                    //Esta función se llama cuando ocurre un fallo.
                    //el objeto DatabaseError es una excepción como las que siempre hemos tratado. Se le puede obtener message, stacktrace...
                    public void onCancelled(DatabaseError error) {
                        // Failed to read value
                        Log.w("error de consulta", "Failed to read value."+ error.toException().getStackTrace());
                    }
                });

                if(u2.getNombre().equals(nombre.getText().toString()) && u2.getPass().equals(pass.getText().toString())) {
                    u2.setCasas(new ArrayList<Casa>());
                    Intent haciaVentanaCasa =new Intent(this, VentanaCasa.class);
                    Bundle datos = new Bundle();
                    datos.putSerializable("usuario", u2);
                    haciaVentanaCasa.putExtras(datos);
                    startActivity(haciaVentanaCasa);
                } else {
                    Toast.makeText(this, "Usuario o contraseña incorrectas ", Toast.LENGTH_SHORT).show();
                }
            }else{
                if(ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                    String[] permisos={Manifest.permission.WRITE_CALENDAR};
                    ActivityCompat.requestPermissions(this, permisos, FlagsOrgHogar.FLAG_PETICION_PERMISO_WRITE_CALENDAR);
                    Toast.makeText(this,"Permiso write calendar",Toast.LENGTH_SHORT).show();
                } if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                    String[] permisos={Manifest.permission.READ_CALENDAR};
                    ActivityCompat.requestPermissions(this, permisos, FlagsOrgHogar.FLAG_PETICION_PERMISO_READ_CALENDAR);
                    Toast.makeText(this,"Permiso read calendar",Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
