package com.example.orghogar.orghogar;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

/**
 * author Amariliz navarro
 */
public class VentanaTusTareas extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ventana_tus_tareas);
    }

    public void clickIrMostrarTareas(View view) {
        Intent btn = new Intent(this, VentanaMostrarTareas.class);
        startActivity(btn);
    }
}
