package com.example.orghogar.orghogar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import java.util.ArrayList;

import clases.Casa;
import clases.Tarea;
import clases.Usuario;

public class VentanaCrearCasa extends AppCompatActivity {
    private Usuario u; //@param u Usuario que ha iniciado sesion en la aplicacion
    private EditText nombreCasa;
    private EditText codigoCasa;
    private ImageButton botonVolver; //@param botonVolver ImageButton para volver a la ventana anterior

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ventana_crear_casa);

        botonVolver = findViewById(R.id.imageButton_atras);

        if (this.getIntent().getExtras() != null) {
            u = (Usuario) this.getIntent().getExtras().getSerializable("usuario");
        } else if (savedInstanceState != null) {
            u = (Usuario) (savedInstanceState.getSerializable("usuario"));
        }
        else {
            u = new Usuario();
        }
    }

    public void clickBotonAtras(View view) {
        Intent i = new Intent(this, VentanaInsertarCasa.class);
        Bundle miBundle = new Bundle();
        miBundle.putSerializable("usuario", u);
        i.putExtras(miBundle);
        startActivity(i);
    }

    /**
     * @param view
     * @TODO hacer que guarde la casa creada en base de datos
     */
    public void clickCrearCasa(View view) {
        nombreCasa = findViewById(R.id.editText_nombreCasa);
        codigoCasa = findViewById(R.id.editText_codigoCasa);
        if(nombreCasa.getText().toString() != "" && codigoCasa.getText().toString() != "") {
            ArrayList<Usuario> usuarios = new ArrayList<>();
            usuarios.add(u);
            Casa casa1 = new Casa(nombreCasa.getText().toString(), usuarios, codigoCasa.getText().toString());
            u.getCasas().add(casa1);

            Intent i = new Intent(this, VentanaCasa.class);
            Bundle miBundle = new Bundle();
            miBundle.putSerializable("usuario", u);
            i.putExtras(miBundle);
            startActivity(i);
        }
    }
}
