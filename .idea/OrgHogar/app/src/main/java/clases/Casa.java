package clases;

import java.io.Serializable;
import java.util.ArrayList;

/** Clase que representa a una casa que tiene tareas y usuarios
 * @author Juan Carlos Beltran
 */
public class Casa implements Serializable {
    private String nombre; //@param nombre que representa el nombre de la casa
    private ArrayList<Usuario> usuarios; //@param usuarios representa la lista de usuarios que estan en la casa
    private ArrayList<Tarea> tareas; //@param tareas representa la lista de tareas que hay en la casa
    private String codigo; //@param codigo representa el codigo unico de la casa

    public Casa() {

    }

    /**
     * Casa (Constructor) construye una casa a partir del nombre, la lista de usuarios, la lista de tareas y el codigo
     * @param nombre String representa el nombre de la casa
     * @param usuarios ArrayList<Usuario> representa la lista de usuarios que estan en la casa
     * @param codigo String representa el codigo unico de la casa
     */
    public Casa(String nombre, ArrayList<Usuario> usuarios, String codigo) {
        this.nombre = nombre;
        this.usuarios = usuarios;
        this.tareas = tareas;
        this.codigo = codigo;
    }

    /**
     * metodo que devuelve el nombre de la casa
     * @return String nombre de la casa
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * metodo que cambia el nombre de la casa
     * @param nombre String por el que se cambia el nombre de la casa
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * metodo que devuelve la lista de usuario que estan en la casa
     * @return ArrayList<Usuario> lista de usuarios
     */
    public ArrayList<Usuario> getUsuarios() {
        return usuarios;
    }

    /**
     * metodo que cambia la lista de usuarios que hay en la casa
     * @param usuarios ArrayList<Usuario> por el que se cambia la lista de usuarios
     */
    public void setUsuarios(ArrayList<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    /**
     * metodo que devuelve la lista de tareas que hay en la casa
     * @return ArrayList<Tarea> lista de tareas
     */
    public ArrayList<Tarea> getTareas() {
        return tareas;
    }

    /**
     * metodo que cambia la lista de tareas que hay en la casa
     * @param tareas ArrayList<Tarea> por la que se cambia la lista de tareas
     */
    public void setTareas(ArrayList<Tarea> tareas) {
        this.tareas = tareas;
    }

    /**
     * metodo que devuelve el codigo de la casa
     * @return String codigo de la casa
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * metodo que cambia el codigo de la casa
     * @param codigo String por el que se cambia el codigo de la casa
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
}
