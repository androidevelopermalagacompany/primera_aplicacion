package clases;

import java.io.Serializable;

/** Clase que representa una tarea que tiene un usuario y que esta en una casa
 * @author Juan Carlos Beltran
 */
public class Tarea implements Serializable {
    private String nombre; //@param nombre que representa el nombre de la tarea
    private String descripcion; //@param descripcion que representa la descripcion de la tarea
    private Usuario encargado; //@param encargado que representa al usuario encargado de la tarea
    private int repeticiones; //@param repeticiones que representa el numero de veces que se debe hacer esa tarea para se eliminada
    private int vecesRepetido; //@param vecesRepetido que representa el numero de veces que se ha hecho ya esa tarea

    /**
     * Tarea (Constructor) construye una tarea a partir de su nombre, descripcion, el usuario encargado y el numero de repeticiones
     * @param nombre String representa el nombre de la tarea
     * @param descripcion String representa la descripcion de la tarea
     * @param encargado Usuario representa el usuario encargado de la tarea
     * @param repeticiones int representa el numero de veces que se debe hacer esa tarea para se eliminada
     */
    public Tarea(String nombre, String descripcion, Usuario encargado, int repeticiones) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.encargado = encargado;
        this.repeticiones = repeticiones;
    }

    /**
     * metodo que devuelve el nombre de la tarea
     * @return String nombre de la tarea
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * metodo que cambia el nombre de la tarea
     * @param nombre String por el que se cambia el nombre de la tarea
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * metodo que devuelve la descripcion de la tarea
     * @return String descripcion de la tarea
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * metodo que cambia la descripcion de la tarea
     * @param descripcion String por el que se cambia la descripcion de la tarea
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * metodo que devuelve el usuario encargado de la tarea
     * @return Usuario encargado de la tarea
     */
    public Usuario getEncargado() {
        return encargado;
    }

    /**
     * metodo que cambia el usuario encargado de la tarea
     * @param encargado Usuario por el que se cambia el encargado de la tarea
     */
    public void setEncargado(Usuario encargado) {
        this.encargado = encargado;
    }

    /**
     * metodo que devuelve el numero de veces que se debe hacer esa tarea para se eliminada
     * @return int el numero de veces que se debe hacer esa tarea para se eliminada
     */
    public int getRepeticiones() {
        return repeticiones;
    }

    /**
     * metodo que cambia el numero de veces que se debe hacer esa tarea para se eliminada
     * @param repeticiones int por el que se cambia el numero de veces que se debe hacer esa tarea para se eliminada
     */
    public void setRepeticiones(int repeticiones) {
        this.repeticiones = repeticiones;
    }

    /**
     * metodo que devuelve el numero de veces que se ha hecho ya esa tarea
     * @return int el numero de veces que se ha hecho ya esa tarea
     */
    public int getVecesRepetido() {
        return vecesRepetido;
    }

    /**
     * metodo que cambia el numero de veces que se ha hecho ya esa tarea
     * @param vecesRepetido int por el que cambia el numero de veces que se ha hecho ya esa tarea
     */
    public void setVecesRepetido(int vecesRepetido) {
        this.vecesRepetido = vecesRepetido;
    }
}
