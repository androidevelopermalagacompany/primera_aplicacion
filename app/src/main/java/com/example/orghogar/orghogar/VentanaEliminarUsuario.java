package com.example.orghogar.orghogar;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.sip.SipSession;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import clases.Usuario;

/** Ventana eliminar usuarios, en donde se elininará a un usuario de nuestra base de datos.
 * @author Carlos Adrián Díaz Prados
 */

public class VentanaEliminarUsuario  extends AppCompatActivity {

    SharedPreferences preferences;

    View dialogView = getLayoutInflater().inflate(R.layout.activity_ventana_eliminar_usuario,null);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ventana_eliminar_usuario);
    }
    public void clickConfirmar(View view) {
        Intent btn = new Intent(this, VentanaEliminarUsuario.class);
        startActivity(btn);
    }



    public void clickUsuario(View view) {
        Intent btn = new Intent(this, VentanaEliminarUsuario.class);
        startActivity(btn);
    }




}








