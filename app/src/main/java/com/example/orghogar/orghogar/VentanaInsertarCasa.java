package com.example.orghogar.orghogar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import java.util.ArrayList;

import clases.Casa;
import clases.Usuario;
import clases.Tarea;

public class VentanaInsertarCasa extends AppCompatActivity {
    private Usuario u; //@param u Usuario que ha iniciado sesion en la aplicacion
    private EditText codigo;
    private ImageButton botonVolver; //@param botonVolver ImageButton para volver a la ventana anterior

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ventana_insertar_casa);

        botonVolver = findViewById(R.id.imageButton7);

        if (this.getIntent().getExtras() != null) {
            u = (Usuario) this.getIntent().getExtras().getSerializable("usuario");
        } else if (savedInstanceState != null) {
            u = (Usuario) (savedInstanceState.getSerializable("usuario"));
        }
        else {
            u = new Usuario();
        }
    }

    public void clickBotonAtras(View view) {
        Intent i = new Intent(this, VentanaCasa.class);
        Bundle miBundle = new Bundle();
        miBundle.putSerializable("usuario", u);
        i.putExtras(miBundle);
        startActivity(i);
    }

    /**
     * @param view
     * @TODO hacer que verifique si el codigo esta en base de datos, si es asi, agregar esa casa al usuario
     */
    public void clickInsertarCasa(View view) {
        codigo = findViewById(R.id.editText_codigo);
        if(codigo.getText().toString() != "") {
            ArrayList<Usuario> usuarios = new ArrayList<>();
            usuarios.add(u);
            Casa casa1 = new Casa("Casa 1234",  usuarios, "codigoDeLaCasa");
            u.getCasas().add(casa1);

            Intent i = new Intent(this, VentanaCasa.class);
            Bundle miBundle = new Bundle();
            miBundle.putSerializable("usuario", u);
            i.putExtras(miBundle);
            startActivity(i);
        }
    }

    public void clickCrearCasa(View view) {
        Intent i = new Intent(this, VentanaCrearCasa.class);
        Bundle miBundle = new Bundle();
        miBundle.putSerializable("usuario", u);
        i.putExtras(miBundle);
        startActivity(i);
    }
}
