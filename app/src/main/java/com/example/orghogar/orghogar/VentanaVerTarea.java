package com.example.orghogar.orghogar;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

/**
 * author Carlos Adrián Díaz Prados
 */
public class VentanaVerTarea extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ventana_ver_tarea);
    }
    public void clickIrMostrarTareas(View view) {
        Intent btn = new Intent(this, VentanaMostrarTareas.class);
        startActivity(btn);
    }
}



