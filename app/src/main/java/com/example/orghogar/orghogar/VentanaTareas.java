package com.example.orghogar.orghogar;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import clases.Tarea;
import clases.Usuario;

/**
 *
 *
 *
 *
 *
 *
 *
 *
 *
 * Esta clase tiene la intencion de crear hasta 4 tareas, he intentado utilizar el metodo que ha usado juanca
 * en la pestaña de casas y crear casas , intentado adaptar el codigo ,
 * por ahora solo esta del todo BIEN los metodos clickAnadirTarea1, y  clickBotonAtras .
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

public class VentanaTareas extends AppCompatActivity {
    private Usuario u; //@param u Usuario que ha iniciado sesion en la aplicacion

    private ImageView image_estrella_1; //@param imagen_estrella_1 ImageView de la tarea 1
    private TextView text_1; //@param text_1 TextView de la tarea 1
    private ImageView image_estrella_2; //@param imagen_estrella_2 ImageView de la tarea 2
    private TextView text_2; //@param text_2 TextView de la tarea 2
    private ImageView image_estrella_3; //@param imagen_estrella_3 ImageView de la tarea 3
    private TextView text_3; //@param text_3 TextView de la casa 3
    private ImageView image_estrella_4; //@param imagen_estrella_4  ImageView de la  tarea 4
    private TextView text_4; //@param text_4 TextView de la tarea 4
    private ImageView botonAnadirTarea;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ventana_tareas);
        botonAnadirTarea = findViewById(R.id.imageView_botonMas);

    }

    public void clickAnadirTarea1(View view) {
        Intent btn=new Intent(this,VentanaAnadirTarea.class);
        startActivity(btn);
    }

    /**
     * Hace un intent que te lleva a la pantalla anterior que es de la clase VentanaMostrarTareas
     * @param view
     */
    public void clickBotonAtras(View view) {
        Intent clickIrMostrarVentanas= new Intent(this, VentanaMostrarTareas.class);
        startActivity(clickIrMostrarVentanas);
    }






}
